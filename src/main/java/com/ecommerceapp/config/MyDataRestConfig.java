package com.ecommerceapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.HttpMethod;

import com.ecommerceapp.entity.Product;
import com.ecommerceapp.entity.ProductCategory;

@Configuration
public class MyDataRestConfig implements RepositoryRestConfigurer{

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		// TODO Auto-generated method stub
		
		HttpMethod[] theUnsupportdAction = {HttpMethod.PUT, HttpMethod.POST, HttpMethod.DELETE};

//		disable HTTP methods for product: Put, Delete, Post
		config.getExposureConfiguration()
			.forDomainType(Product.class)
			.withItemExposure((metdata, httpMethods) -> httpMethods.disable(theUnsupportdAction))
			.withCollectionExposure((metdata, httpMethods) ->httpMethods.disable(theUnsupportdAction));
		
//		disable HTTP methods for product Category: Put, Delete, Post
		config.getExposureConfiguration()
			.forDomainType(ProductCategory.class)
			.withItemExposure((metdata, httpMethods) -> httpMethods.disable(theUnsupportdAction))
			.withCollectionExposure((metdata, httpMethods) ->httpMethods.disable(theUnsupportdAction));
		
	}
}
