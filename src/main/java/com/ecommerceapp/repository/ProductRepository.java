package com.ecommerceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.ecommerceapp.entity.Product;

@CrossOrigin
public interface ProductRepository extends JpaRepository<Product, Long> {

}
